### 18.0.0 Update to Angular 18
* 15f2e85 -- [CI/CD] Update packages.json version based on GitLab tag.
*   20c1f7c -- Merge branch 'develop' into 'master'
|\  
| * ae60aac -- Force update via VERSION.txt
| * 0cea275 -- Update pipeline
* | af5e739 -- Merge branch 'develop' into 'master'
|\| 
| * 62c7b9e -- Merge branch '19-update-to-angular-18' into 'develop'
|/| 
| * 24a1452 -- Revert eslintrc update
| * 06e01bd -- Revert eslint changes
| * ad3edd4 -- Add updated eslint config
| * 35f100a -- Update to angular 18
|/  
* f40ef29 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e42728f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ea55ca5 -- Merge branch '18-update-to-angular-16' into 'master'
|\  
| * 36d1609 -- Update to Angular 16
|/  
* 0cb7f96 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4a5e74e -- [CI/CD] Update packages.json version based on GitLab tag.
*   6d77ecb -- Merge branch '17-update-to-angular-15' into 'master'
|\  
| * 74b540b -- Update to Angular 15
|/  
* 7aebf6b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16a2f0c -- [CI/CD] Update packages.json version based on GitLab tag.
*   e1243e3 -- Merge branch '16-update-to-angular-14' into 'master'
|\  
| * 8b59c86 -- Resolve "Update to Angular 14"
|/  
* aab75e2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0436481 -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d4a797 -- Merge branch '15-update-to-angular-13' into 'master'
|\  
| * 5842da4 -- Resolve "Update to Angular 13"
|/  
*   e27eaad -- Merge branch '14-add-license-file' into 'master'
|\  
| * b74956e -- Add license file
|/  
* caa571d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* af6bb29 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8e5fc7f -- Merge branch '13-rollback-version' into 'master'
|\  
| * 18d5714 -- Update changelog
|/  
*   1658f06 -- Merge branch '12-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * acff678 -- Update gitlab CI config
|/  
* 60cfbdc -- Update project package.json version based on GitLab tag. Done by CI
*   101882e -- Merge branch '11-update-to-angular-12' into 'master'
|\  
| * 656ee0f -- Resolve "Update to Angular 12"
|/  
* ac1d845 -- Update project package.json version based on GitLab tag. Done by CI
*   5d6846e -- Merge branch '10-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0a3788e -- peerDependency update
|/  
* d009e06 -- Update project package.json version based on GitLab tag. Done by CI
*   5f3f127 -- Merge branch '9-update-to-angular-11' into 'master'
|\  
| * cf3b48e -- Update to Angular 11
|/  
*   d95b0a3 -- Merge branch '8-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * f3bb149 -- recreate package lock
|/  
* 07736da -- Update project package.json version based on GitLab tag. Done by CI
*   5f7cd7a -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 404f6af -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   a6ca74b -- Merge branch '6-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 3046df9 -- Resolve "Migrate from tslint to eslint"
|/  
* 48795a7 -- Update project package.json version based on GitLab tag. Done by CI
*   a9cdcb7 -- Merge branch '5-rename-package-to-kypo-user-and-group-model' into 'master'
|\  
| * b272603 -- Rename to @kypo/user-and-group-model
|/  
*   b2ef7cc -- Merge branch '4-use-cypress-image-in-ci' into 'master'
|\  
| * 24fa163 -- Resolve "Use cypress image in CI"
|/  
* 175bd89 -- Update project package.json version based on GitLab tag. Done by CI
*   001dafd -- Merge branch '3-update-to-angular-10' into 'master'
|\  
| * 2cea8c6 -- Resolve "Update to Angular 10"
|/  
*   6698195 -- Merge branch '2-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 307722d -- Update .gitlab-ci.yml
|/  
* c8a0eba -- Merge branch '1-add-ci' into 'master'
### 16.0.0 Update to Angular 16.
* e42728f -- [CI/CD] Update packages.json version based on GitLab tag.
*   ea55ca5 -- Merge branch '18-update-to-angular-16' into 'master'
|\  
| * 36d1609 -- Update to Angular 16
|/  
* 0cb7f96 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 4a5e74e -- [CI/CD] Update packages.json version based on GitLab tag.
*   6d77ecb -- Merge branch '17-update-to-angular-15' into 'master'
|\  
| * 74b540b -- Update to Angular 15
|/  
* 7aebf6b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16a2f0c -- [CI/CD] Update packages.json version based on GitLab tag.
*   e1243e3 -- Merge branch '16-update-to-angular-14' into 'master'
|\  
| * 8b59c86 -- Resolve "Update to Angular 14"
|/  
* aab75e2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0436481 -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d4a797 -- Merge branch '15-update-to-angular-13' into 'master'
|\  
| * 5842da4 -- Resolve "Update to Angular 13"
|/  
*   e27eaad -- Merge branch '14-add-license-file' into 'master'
|\  
| * b74956e -- Add license file
|/  
* caa571d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* af6bb29 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8e5fc7f -- Merge branch '13-rollback-version' into 'master'
|\  
| * 18d5714 -- Update changelog
|/  
*   1658f06 -- Merge branch '12-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * acff678 -- Update gitlab CI config
|/  
* 60cfbdc -- Update project package.json version based on GitLab tag. Done by CI
*   101882e -- Merge branch '11-update-to-angular-12' into 'master'
|\  
| * 656ee0f -- Resolve "Update to Angular 12"
|/  
* ac1d845 -- Update project package.json version based on GitLab tag. Done by CI
*   5d6846e -- Merge branch '10-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0a3788e -- peerDependency update
|/  
* d009e06 -- Update project package.json version based on GitLab tag. Done by CI
*   5f3f127 -- Merge branch '9-update-to-angular-11' into 'master'
|\  
| * cf3b48e -- Update to Angular 11
|/  
*   d95b0a3 -- Merge branch '8-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * f3bb149 -- recreate package lock
|/  
* 07736da -- Update project package.json version based on GitLab tag. Done by CI
*   5f7cd7a -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 404f6af -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   a6ca74b -- Merge branch '6-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 3046df9 -- Resolve "Migrate from tslint to eslint"
|/  
* 48795a7 -- Update project package.json version based on GitLab tag. Done by CI
*   a9cdcb7 -- Merge branch '5-rename-package-to-kypo-user-and-group-model' into 'master'
|\  
| * b272603 -- Rename to @kypo/user-and-group-model
|/  
*   b2ef7cc -- Merge branch '4-use-cypress-image-in-ci' into 'master'
|\  
| * 24fa163 -- Resolve "Use cypress image in CI"
|/  
* 175bd89 -- Update project package.json version based on GitLab tag. Done by CI
*   001dafd -- Merge branch '3-update-to-angular-10' into 'master'
|\  
| * 2cea8c6 -- Resolve "Update to Angular 10"
|/  
*   6698195 -- Merge branch '2-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 307722d -- Update .gitlab-ci.yml
|/  
* c8a0eba -- Merge branch '1-add-ci' into 'master'
### 15.0.0 Update to Angular 15.
* 4a5e74e -- [CI/CD] Update packages.json version based on GitLab tag.
*   6d77ecb -- Merge branch '17-update-to-angular-15' into 'master'
|\  
| * 74b540b -- Update to Angular 15
|/  
* 7aebf6b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 16a2f0c -- [CI/CD] Update packages.json version based on GitLab tag.
*   e1243e3 -- Merge branch '16-update-to-angular-14' into 'master'
|\  
| * 8b59c86 -- Resolve "Update to Angular 14"
|/  
* aab75e2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0436481 -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d4a797 -- Merge branch '15-update-to-angular-13' into 'master'
|\  
| * 5842da4 -- Resolve "Update to Angular 13"
|/  
*   e27eaad -- Merge branch '14-add-license-file' into 'master'
|\  
| * b74956e -- Add license file
|/  
* caa571d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* af6bb29 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8e5fc7f -- Merge branch '13-rollback-version' into 'master'
|\  
| * 18d5714 -- Update changelog
|/  
*   1658f06 -- Merge branch '12-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * acff678 -- Update gitlab CI config
|/  
* 60cfbdc -- Update project package.json version based on GitLab tag. Done by CI
*   101882e -- Merge branch '11-update-to-angular-12' into 'master'
|\  
| * 656ee0f -- Resolve "Update to Angular 12"
|/  
* ac1d845 -- Update project package.json version based on GitLab tag. Done by CI
*   5d6846e -- Merge branch '10-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0a3788e -- peerDependency update
|/  
* d009e06 -- Update project package.json version based on GitLab tag. Done by CI
*   5f3f127 -- Merge branch '9-update-to-angular-11' into 'master'
|\  
| * cf3b48e -- Update to Angular 11
|/  
*   d95b0a3 -- Merge branch '8-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * f3bb149 -- recreate package lock
|/  
* 07736da -- Update project package.json version based on GitLab tag. Done by CI
*   5f7cd7a -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 404f6af -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   a6ca74b -- Merge branch '6-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 3046df9 -- Resolve "Migrate from tslint to eslint"
|/  
* 48795a7 -- Update project package.json version based on GitLab tag. Done by CI
*   a9cdcb7 -- Merge branch '5-rename-package-to-kypo-user-and-group-model' into 'master'
|\  
| * b272603 -- Rename to @kypo/user-and-group-model
|/  
*   b2ef7cc -- Merge branch '4-use-cypress-image-in-ci' into 'master'
|\  
| * 24fa163 -- Resolve "Use cypress image in CI"
|/  
* 175bd89 -- Update project package.json version based on GitLab tag. Done by CI
*   001dafd -- Merge branch '3-update-to-angular-10' into 'master'
|\  
| * 2cea8c6 -- Resolve "Update to Angular 10"
|/  
*   6698195 -- Merge branch '2-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 307722d -- Update .gitlab-ci.yml
|/  
* c8a0eba -- Merge branch '1-add-ci' into 'master'
### 14.0.0 Update to Angular 14.
* 16a2f0c -- [CI/CD] Update packages.json version based on GitLab tag.
*   e1243e3 -- Merge branch '16-update-to-angular-14' into 'master'
|\  
| * 8b59c86 -- Resolve "Update to Angular 14"
|/  
* aab75e2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 0436481 -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d4a797 -- Merge branch '15-update-to-angular-13' into 'master'
|\  
| * 5842da4 -- Resolve "Update to Angular 13"
|/  
*   e27eaad -- Merge branch '14-add-license-file' into 'master'
|\  
| * b74956e -- Add license file
|/  
* caa571d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* af6bb29 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8e5fc7f -- Merge branch '13-rollback-version' into 'master'
|\  
| * 18d5714 -- Update changelog
|/  
*   1658f06 -- Merge branch '12-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * acff678 -- Update gitlab CI config
|/  
* 60cfbdc -- Update project package.json version based on GitLab tag. Done by CI
*   101882e -- Merge branch '11-update-to-angular-12' into 'master'
|\  
| * 656ee0f -- Resolve "Update to Angular 12"
|/  
* ac1d845 -- Update project package.json version based on GitLab tag. Done by CI
*   5d6846e -- Merge branch '10-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0a3788e -- peerDependency update
|/  
* d009e06 -- Update project package.json version based on GitLab tag. Done by CI
*   5f3f127 -- Merge branch '9-update-to-angular-11' into 'master'
|\  
| * cf3b48e -- Update to Angular 11
|/  
*   d95b0a3 -- Merge branch '8-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * f3bb149 -- recreate package lock
|/  
* 07736da -- Update project package.json version based on GitLab tag. Done by CI
*   5f7cd7a -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 404f6af -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   a6ca74b -- Merge branch '6-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 3046df9 -- Resolve "Migrate from tslint to eslint"
|/  
* 48795a7 -- Update project package.json version based on GitLab tag. Done by CI
*   a9cdcb7 -- Merge branch '5-rename-package-to-kypo-user-and-group-model' into 'master'
|\  
| * b272603 -- Rename to @kypo/user-and-group-model
|/  
*   b2ef7cc -- Merge branch '4-use-cypress-image-in-ci' into 'master'
|\  
| * 24fa163 -- Resolve "Use cypress image in CI"
|/  
* 175bd89 -- Update project package.json version based on GitLab tag. Done by CI
*   001dafd -- Merge branch '3-update-to-angular-10' into 'master'
|\  
| * 2cea8c6 -- Resolve "Update to Angular 10"
|/  
*   6698195 -- Merge branch '2-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 307722d -- Update .gitlab-ci.yml
|/  
* c8a0eba -- Merge branch '1-add-ci' into 'master'
### 13.0.0 Update to Angular 13, CI/CD optimization
* 0436481 -- [CI/CD] Update packages.json version based on GitLab tag.
*   0d4a797 -- Merge branch '15-update-to-angular-13' into 'master'
|\  
| * 5842da4 -- Resolve "Update to Angular 13"
|/  
*   e27eaad -- Merge branch '14-add-license-file' into 'master'
|\  
| * b74956e -- Add license file
|/  
* caa571d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* af6bb29 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8e5fc7f -- Merge branch '13-rollback-version' into 'master'
|\  
| * 18d5714 -- Update changelog
|/  
*   1658f06 -- Merge branch '12-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * acff678 -- Update gitlab CI config
|/  
* 60cfbdc -- Update project package.json version based on GitLab tag. Done by CI
*   101882e -- Merge branch '11-update-to-angular-12' into 'master'
|\  
| * 656ee0f -- Resolve "Update to Angular 12"
|/  
* ac1d845 -- Update project package.json version based on GitLab tag. Done by CI
*   5d6846e -- Merge branch '10-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0a3788e -- peerDependency update
|/  
* d009e06 -- Update project package.json version based on GitLab tag. Done by CI
*   5f3f127 -- Merge branch '9-update-to-angular-11' into 'master'
|\  
| * cf3b48e -- Update to Angular 11
|/  
*   d95b0a3 -- Merge branch '8-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * f3bb149 -- recreate package lock
|/  
* 07736da -- Update project package.json version based on GitLab tag. Done by CI
*   5f7cd7a -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 404f6af -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   a6ca74b -- Merge branch '6-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 3046df9 -- Resolve "Migrate from tslint to eslint"
|/  
* 48795a7 -- Update project package.json version based on GitLab tag. Done by CI
*   a9cdcb7 -- Merge branch '5-rename-package-to-kypo-user-and-group-model' into 'master'
|\  
| * b272603 -- Rename to @kypo/user-and-group-model
|/  
*   b2ef7cc -- Merge branch '4-use-cypress-image-in-ci' into 'master'
|\  
| * 24fa163 -- Resolve "Use cypress image in CI"
|/  
* 175bd89 -- Update project package.json version based on GitLab tag. Done by CI
*   001dafd -- Merge branch '3-update-to-angular-10' into 'master'
|\  
| * 2cea8c6 -- Resolve "Update to Angular 10"
|/  
*   6698195 -- Merge branch '2-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 307722d -- Update .gitlab-ci.yml
|/  
* c8a0eba -- Merge branch '1-add-ci' into 'master'
### 12.0.2 Update gitlab CI config
* af6bb29 -- [CI/CD] Update packages.json version based on GitLab tag.
*   8e5fc7f -- Merge branch '13-rollback-version' into 'master'
|\  
| * 18d5714 -- Update changelog
|/  
*   1658f06 -- Merge branch '12-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * acff678 -- Update gitlab CI config
|/  
* 60cfbdc -- Update project package.json version based on GitLab tag. Done by CI
*   101882e -- Merge branch '11-update-to-angular-12' into 'master'
|\  
| * 656ee0f -- Resolve "Update to Angular 12"
|/  
* ac1d845 -- Update project package.json version based on GitLab tag. Done by CI
*   5d6846e -- Merge branch '10-update-peerdependencies-to-angular-11' into 'master'
|\  
| * 0a3788e -- peerDependency update
|/  
* d009e06 -- Update project package.json version based on GitLab tag. Done by CI
*   5f3f127 -- Merge branch '9-update-to-angular-11' into 'master'
|\  
| * cf3b48e -- Update to Angular 11
|/  
*   d95b0a3 -- Merge branch '8-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * f3bb149 -- recreate package lock
|/  
* 07736da -- Update project package.json version based on GitLab tag. Done by CI
*   5f7cd7a -- Merge branch '7-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 404f6af -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   a6ca74b -- Merge branch '6-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 3046df9 -- Resolve "Migrate from tslint to eslint"
|/  
* 48795a7 -- Update project package.json version based on GitLab tag. Done by CI
*   a9cdcb7 -- Merge branch '5-rename-package-to-kypo-user-and-group-model' into 'master'
|\  
| * b272603 -- Rename to @kypo/user-and-group-model
|/  
*   b2ef7cc -- Merge branch '4-use-cypress-image-in-ci' into 'master'
|\  
| * 24fa163 -- Resolve "Use cypress image in CI"
|/  
* 175bd89 -- Update project package.json version based on GitLab tag. Done by CI
*   001dafd -- Merge branch '3-update-to-angular-10' into 'master'
|\  
| * 2cea8c6 -- Resolve "Update to Angular 10"
|/  
*   6698195 -- Merge branch '2-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 307722d -- Update .gitlab-ci.yml
|/  
* c8a0eba -- Merge branch '1-add-ci' into 'master'
