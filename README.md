# KYPO User and Group Model

This library contains frontend model of [KYPO User and Group service](https://gitlab.ics.muni.cz/kypo-crp/backend-java/kypo2-user-and-group).

## Prerequisites
To use the library you need to have installed:
* NPM with access to [KYPO registry](https://projects.ics.muni.cz/projects/kbase/knowledgebase/articles/153)

## Usage
To use the model in your project follow these steps:
1. Run `npm install @muni-kypo-crp/user-and-group-model`
2. Import classes like usual, for example `import { Group } from '@muni-kypo-crp/user-and-group-model`
